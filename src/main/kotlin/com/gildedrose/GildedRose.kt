package com.gildedrose

class GildedRose(var items: Array<Item>) {
    private val qualityUpdater = QualityUpdater()
    fun updateQuality() {
        for (i in items.indices) {
            qualityUpdater.update(items[i])
            if (items[i].name == "Aged Brie") {
                updateAgedBrie(items[i]); continue }
            if (items[i].name == "Backstage passes to a TAFKAL80ETC concert") {
                updateBackstagePass(items[i]); continue }
            if (items[i].name == "Sulfuras, Hand of Ragnaros") {
                updateSulfuras(items[i]); continue }
            updateGenericItem(items[i])
        }
    }

    private fun updateAgedBrie(item: Item) {
        if (item.quality < 50) { item.quality = item.quality + 1 }

        item.sellIn = item.sellIn - 1

        if (item.sellIn < 0 && item.quality < 50) { item.quality = item.quality + 1 }
    }

    private fun updateBackstagePass(item: Item) {
        if (item.quality < 50) {
            item.quality = item.quality + 1

            if (item.sellIn < 11 && item.quality < 50) { item.quality = item.quality + 1 }
            if (item.sellIn < 6 && item.quality < 50) { item.quality = item.quality + 1 }
        }

        item.sellIn = item.sellIn - 1

        if (item.sellIn < 0) { item.quality = item.quality - item.quality }
    }

    private fun updateSulfuras(item: Item) {
    }

    private fun updateGenericItem(item: Item) {
        if (item.quality > 0) { item.quality = item.quality - 1 }

        item.sellIn = item.sellIn - 1

        if (item.sellIn < 0 && item.quality > 0) { item.quality = item.quality - 1 }
    }
}

class QualityUpdater {
    fun update(item: Item) {
        TODO("Not yet implemented")
    }

}