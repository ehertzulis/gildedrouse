package com.gildedrose

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.io.File

class MainTest {
    @Test
    fun `main should output expected result`() {
        val expected = File(javaClass.classLoader.getResource("output.txt")!!.file).readText();
        var actual = ""

        actual += "OMGHAI!\n"

        val items = arrayOf(Item("+5 Dexterity Vest", 10, 20), //
            Item("Aged Brie", 2, 0), //
            Item("Elixir of the Mongoose", 5, 7), //
            Item("Sulfuras, Hand of Ragnaros", 0, 80), //
            Item("Sulfuras, Hand of Ragnaros", -1, 80),
            Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
            Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
            Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
            // this conjured item does not work properly yet
            Item("Conjured Mana Cake", 3, 6))

        val app = GildedRose(items)

        val days = 30

        for (i in 0..days - 1) {
            actual += "-------- day $i --------\n"
            actual += "name, sellIn, quality\n"
            for (item in items) { actual += "$item\n" }
            actual += "\n"
            app.updateQuality()
        }

        assertEquals(actual, expected)
    }
}
